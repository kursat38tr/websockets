const express = require('express');
const app = express();
const socketIo = require('socket.io')

let namespaces = require('./data/namespaces');
// console.log(namespaces);

app.use(express.static(__dirname + '/public'));

const expressServer = app.listen(9000);
const io = socketIo(expressServer);


io.on('connection', (socket) => {
    console.log(socket.handshake);
    // build an array to send back with the img and endpoint for each NS
    let nsData = namespaces.map((ns)=>{
        return{
            img: ns.img,
            endpoint: ns.endpoint
        }
    })
    
    // Send the nsData back to the clientInformation, We need to use 
    // socket, Not io, beacause we want it to go to just the client

    socket.emit('nsList', nsData);
})

 
namespaces.forEach((namespace)=>{
    // console.log(namespace);
    io.of(namespace.endpoint).on('connection', (nsSocket) => {
        console.log(nsSocket.handshake);
        const username = nsSocket.handshake.query.username;
        // console.log(`${nsSocket.id} has join ${namespace.endpoint}`);
        // a socket has connected to one of the namespaces
        // send that ns group info back
        nsSocket.emit('nsRoomLoad', namespace.rooms);
        nsSocket.on('joinRoom',(roomToJoin, numberOfUsersCallback)=>{
            console.log(nsSocket.rooms);
            const roomToLeave = Object.keys(nsSocket.rooms)[1];
            nsSocket.leave(roomToLeave);
            updateUsersInRooms(namespace, roomToLeave);
            nsSocket.join(roomToJoin);

            // io.of('wiki').in(roomToJoin).clients((error, clients)=>{
            //     console.log(clients.length);
            //     numberOfUsersCallback(clients.length);
            // })
            const nsRoom = namespace.rooms.find((room)=>{
                return room.roomTitle === roomToJoin;
            });
            nsSocket.emit('historyCatchup',nsRoom.history );
            updateUsersInRooms(namespace, roomToJoin);
        })
        nsSocket.on('newMessageToServer', (msg)=>{
            const fullMsg = {
                text: msg.text,
                time: Date.now(),
                username: username,
                avatar: 'http://via.placeholder.com/30'
            }
            // console.log(fullMsg);
            // Send this message to All the sockets that are in the room that THIS socket is in.
            // How we cna we find what room this socket is in.
            // console.log(nsSocket.rooms);

            /*The user will be in the 2de room in the object list
            This is beacyse the socket is always joins its own room connection
            Get the key*/

            const roomTitle = Object.keys(nsSocket.rooms)[1];

            // We need to find the room object for this room
            const nsRoom = namespace.rooms.find((room)=>{
                return room.roomTitle === roomTitle;
            })
            // console.log("This room")
            // console.log(nsRoom);
            nsRoom.addMessage(fullMsg);
            io.of(namespace.endpoint).to(roomTitle).emit('messageToClients', fullMsg);
        });
    });
});

function updateUsersInRooms(namespace, roomToJoin){
    // Send back the number of users to aal sockets connected to this room

    io.of(namespace.endpoint).in(roomToJoin).clients((error, clients)=>{
        // console.log(`There are ${clients.length} in this room`);
        io.of(namespace.endpoint).in(roomToJoin).emit('updateMembers', clients.length);
    })
}