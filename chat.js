const express = require('express');
const app = express();
const socketio = require('socket.io')

app.use(express.static(__dirname + '/public'));

const expressServer = app.listen(9000);
const io = socketio(expressServer);
io.on('connection', (socket) => {
    socket.emit('messageFromServer', { data: " Welcome to the server" })
    socket.on('messageFromServer', (dataFromClient) => {
        console.log(dataFromClient);
    })
    socket.join('level1');
    socket.to('level1').emit('joined', `${socket.id} says I Have joined the level 1 room!`);
})

io.of('/admin').on('connect ion',(socket)=>{
    console.log("Someone connected to the admin namespace");
    io.of('/admin').emit('welcome', "Welcome to the admin channel")
})   



// io.on('connection', (socket) => {
//     socket.emit('messageFromServer', { data: " Welcome to the server" })
//     socket.on('messageFromServer', (dataFromClient) => {
//         console.log(dataFromClient);
//     })
//     // socket.join('level1');
//     // socket.to('level1').emit('joined', `${socket.id} says I Have joined the level 1 room!`);

//     socket.on('newMessageToServer', (msg)=>{
//         io.of('/').emit('messageToClients', {text:msg.text})
//     })

//     setTimeout(()=>{
//         io.of('/admin').emit('welcome', "Welcome to the admin channel, from the main channel")
//     },2000)
// })
